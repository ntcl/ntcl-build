#!/usr/bin/env python3
import sys

def create_class(name):
    s = """module %(name)s_module
    implicit none
    private

    public :: %(name)s

    type :: %(name)s
    contains
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type %(name)s

    interface %(name)s
        module procedure constructor
    end interface %(name)s

contains
    function constructor() result(this)
        type(%(name)s) :: this

        call this%%clear()
    end function constructor

    subroutine cleanup(this)
        class(%(name)s), intent(inout) :: this

        call this%%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(%(name)s), intent(inout) :: this
    end subroutine clear
end module %(name)s_module""" % {"name" : name}
    return s

if __name__ == "__main__":
    usage = "python %s class_name" % sys.argv[0]

    try:
        name = sys.argv[1]
    except:
        print(usage)
        sys.exit(1)

    print(create_class(name))

