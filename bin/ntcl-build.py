#! /usr/bin/env python3

import os
import git
import subprocess
import io
import glob

from ntcl_build_tools import BuildSystem, BuildEnvironment, RepositoryManager

default_systemd = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "system.d/"))

build = "ntcl-build"
util = "ntcl-util"
data = "ntcl-data"
tensor = "ntcl-tensor"
algorithms = "ntcl-algorithms"
examples = "ntcl-examples"
url_base="git@gitlab.com:ntcl/"

def run_make(directory, env, target, dryrun):
    print(f"Running 'make {target}' in {directory}")
    cmd = env.create_environment_script(directory)
    cmd += f"make {target}\n"

    print(f"Command script:\n{cmd}")

    if not dryrun:
        subprocess.run(cmd, shell=True, executable='/bin/bash', env=env.build_env, check=True)

def run_make_in_all_dirs(dirs, env, target, dryrun):
    for d in dirs: run_make(d, env, target, dryrun)

def prepare_and_compile_code(args, manager):

    if args.systemfile is not None:
        system = BuildSystem.fromFile(args.systemfile, args)
    else:
        system = BuildSystem(args.system, args.path, args)
    print(f"Compiling for system: {system}")

    system.check_for_required()

    build_directory = os.path.realpath(args.build_directory)
    env = BuildEnvironment.create_from_components(system, build_directory)
    dirs = manager.get_all_repository_directories()

    if args.clean: run_make_in_all_dirs(dirs, env, "clean", args.dryrun)
    run_make_in_all_dirs(dirs, env, "libraries", args.dryrun)
    if args.tests: run_make_in_all_dirs(dirs, env, "test", args.dryrun)
    run_make_in_all_dirs(dirs, env, "apps", args.dryrun)

    print_build_receipt(manager, env)

def print_build_receipt(manager, env):
    date_output = subprocess.run(["date"], stdout=subprocess.PIPE)
    dirs = manager.get_all_repository_directories()
    git_outputs = []
    for d in dirs:
        git_outputs.append(subprocess.run(["git", "-C", d, "rev-parse", "HEAD"], stdout=subprocess.PIPE))
    link_output = subprocess.Popen(["ldd", "ntcl-examples/bin/timed_tc.x"], stdout=subprocess.PIPE)


    with open('build_receipt.txt', 'w') as build_file:
        # Array bounds trim output for neatness
        print("date: ", str(date_output.stdout)[2:-3], file=build_file)
        print(f"args:\n{args}\n\n", file=build_file)
        for d in dirs:
            print(f"Running 'make in {d}", file=build_file)
            cmd = env.create_environment_script(d)
            print(f"Command script:\n{cmd}", file=build_file)

        print("\n\nldd ntcl-examples/bin/timed_tc.x", file=build_file)
        for line in io.TextIOWrapper(link_output.stdout, encoding="utf-8"):
            print(line, file=build_file)
        print("\n\nlink outputs:\n", str(link_output.stdout)[2:-3], file=build_file)
        for d, git_output in zip(dirs, git_outputs):
            print(d, file=build_file)
            print("commit hash: ", str(git_output.stdout)[2:-3], file=build_file)

if __name__ == "__main__":
    import argparse, sys
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()

    group.add_argument("-s", "--system", default="default",
            type=str.lower, help="System to compile for.")
    group.add_argument("-f", "--systemfile", help="Specify the system file to use.")

    parser.add_argument("--path", default=default_systemd, help="Path to look for system files.")
    parser.add_argument("-b", "--build_directory", default=os.getcwd(), help="Directory to use for builds.")
    parser.add_argument("-r", "--release", default="main", help="Release to use for build.")
    parser.add_argument("-u", "--update", help="Update source.", action="store_true")
    parser.add_argument("-c", "--compile", help="Compile source.", action="store_true")
    parser.add_argument("-cl", "--clean", help="Compile in a clean source tree", action="store_true")
    parser.add_argument("-t", "--tests", help="Compile tests.", action="store_true")
    parser.add_argument("-l", "--list_systems", help="List available systems and exits.", action="store_true")
    parser.add_argument("-d", "--debug", help="Compile with debug flags.", action="store_true")
    parser.add_argument("-p", "--profile", help="Compile with profile flags.", action="store_true")
    parser.add_argument("-n", "--dryrun", help="Dry run, does nothing but print messages.", action="store_true")
    parser.add_argument("-ar", "--additional_repositories",
            help="List of additional repositories to clone and compile (comma separated)")
    parser.add_argument("-o", "--only", help="Only compile these repositories")


    args = parser.parse_args()

    if args.list_systems:
        BuildSystem.list_available(os.path.abspath(args.path))
        sys.exit(0)

    manager = RepositoryManager.from_args(args, url_base, [util, data, tensor, algorithms, examples])

    manager.update_all()
    if args.compile: prepare_and_compile_code(args, manager)
