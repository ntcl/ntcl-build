NV_COMPILER =  nvcc
NV_STDFLAGS = -fdiagnostics-color=always, -Wall, -Wextra, -ansi, -std=${CSTANDARD}
NV_DEBUGFLAGS = ${NV_STDFLAGS}, -O0, -g, -fbounds-check, -ftrapv, -rdynamic
NV_PRODFLAGS = ${NV_STDFLAGS}, -O3
CUDA_DEBUGFLAGS = -g -G
