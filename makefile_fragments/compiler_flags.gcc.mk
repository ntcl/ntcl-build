COMPILER = gfortran
CCOMPILER = gcc
modcmd = -J
ldcmd = -fuse-ld=
STANDARD = -std=f2008ts
STD_FLAGS = -fdiagnostics-color=always -Wall -Wextra -cpp
GCC10_FFLAGS = -fallow-argument-mismatch
cmd = gfortran --version | grep ^"GNU Fortran" | sed 's/^.*) //g' | sed 's/ \+.*//g'|sed 's/\.\+.*//g'
GCCVERSION := $(shell ${cmd})
ifeq ($(shell expr ${GCCVERSION} \>= 10), 1)
	STD_FLAGS += ${GCC10_FFLAGS}
	CSTANDARD = c++17
else
	CSTANDARD = c++11
endif
STD_CFLAGS = -Wall -Wextra
OPENMP_FLAGS =-fopenmp
PROD_FLAGS = -O3
DEBUG_FLAGS = -Og -g -ggdb -fcheck=all -Wuse-without-only -fbacktrace -ffpe-trap=invalid,zero,overflow
DEBUG_CFLAGS = -Og -g -ggdb -pedantic-errors -fcheck=all
PROFILE_FLAGS = -pg
STATIC_FLAGS = -static
OPTINFO_FLAGS= -fopt-info-all
