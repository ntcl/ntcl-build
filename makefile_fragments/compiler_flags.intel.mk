COMPILER = ifort
CCOMPILER = icc
modcmd = -module 
STD_FLAGS = -heap-arrays 1024 -fltconsistency
OPENMP_FLAGS =-qopenmp
PROD_FLAGS = -O2
DEBUG_FLAGS = -Od -debug all -g
