#!/usr/bin/env python3

import os

class BuildEnvironment(object):
    def __init__(this, system, directory):
        this.system = system
        this.build_env = {**os.environ, 'NTCL_ROOT' : directory}

    def create_environment_script(this, directory):
        s = f"cd {directory}\n"
        if this.system.has_modules():
            s += 'module load ' + this.system.get_modules() + '\n'
        for key, value in this.system.environment_variables.items():
            s += f'export {key}="{value}"\n'

        macros = 'export NTCL_MACRO_FLAGS="'
        for macro in this.system.macros:
            macros += f'-D {macro} '
        key = "GLOBAL_SCRATCH_BUFFER_SIZE"
        if key in this.system.environment_variables.keys():
            item = this.system.environment_variables[key]
            macros += f'-D {key}={item} '

        macros += f'"'

        s += macros + '\n'
        return s

    @staticmethod
    def create_from_components(system, build_directory):
        return BuildEnvironment(system, build_directory)
