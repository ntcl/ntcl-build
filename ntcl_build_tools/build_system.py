#!/usr/bin/env python3

import os
import glob

class BuildSystem(object):
    def __init__(this, name, path, args=None):
        this.name = name
        this.modules = []
        this.environment_variables = {}
        this.required_env_variables = []
        this.macros = []

        this.parse_system_file(os.path.join(path, this.name))
        if args is not None:
            this.define_variable("DEBUG", args.debug)
            this.define_variable("PROFILE", args.profile)

    def check_for_required(this):
        for key in this.required_env_variables:
            if key not in os.environ.keys():
                raise KeyError(f"Required environment variables are not defined: {this.get_required_env_variables()}")

    def parse_system_file(this, filename):
        with open(filename, 'r') as fh:
            lines = fh.readlines()

        for line in lines:
            line = line.partition('#')[0]
            try:
                key, value = [x.strip() for x in line.split('=', 1)]
                if key == "modules": this.modules.extend(value.split())
                if key == "required_env_variables": this.required_env_variables.extend(value.split())
                if key == "macros": this.macros.extend(value.split())
                else: this.environment_variables[key] = value
            except: continue

    def has_modules(this): return len(this.modules) > 0
    def get_modules(this): return ' '.join(this.modules)
    def get_required_env_variables(this): return ' '.join(this.required_env_variables)
    def get_macros(this): return ' '.join(this.macros)

    def define_variable(this, name, define):
        if define: this.environment_variables[name] = "1"

    def __str__(this):
        s = f"BuildSystem: {this.name}\n"
        if len(this.modules) > 0: s += '  modules: ' + this.get_modules() + '\n'
        if len(this.required_env_variables) > 0: s += '  required_env_variables: ' + this.get_required_env_variables() + '\n'
        for key, value in this.environment_variables.items():
            s += f'  {key}: {value}\n'

        if len(this.macros) > 0: s += '  macros: ' + this.get_macros() + '\n'
        return s[:-1]

    @staticmethod
    def fromFile(filename, args):
        path, name = os.path.split(os.path.abspath(filename))
        return BuildSystem(name, path, args)

    @staticmethod
    def get_available(path):
        return [os.path.basename(x) for x in glob.glob(f"{path}/*")]

    @staticmethod
    def list_available(path):
        print("Available systems:")
        for t in [BuildSystem(x, path) for x in BuildSystem.get_available(path)]: print(t)
