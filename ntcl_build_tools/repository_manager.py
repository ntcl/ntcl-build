#!/usr/bin/env python3

import os
import git

class RepositoryManager(object):
    def __init__(this, url_base, build_directory, release, update, list_of_repository_names):
        this.url_base = url_base
        this.build_directory = build_directory
        this.release = release
        this.update = update
        this.repository_names = list_of_repository_names

    def create_directory_if_needed(this, directory):
        try:
            os.mkdir(directory)
        except:
            if os.path.isdir(directory):
                print(f"Directory already exists: {directory}")
                return
            raise
        print(f"Created directory: {directory}")

    def get_repository_directory(this, directory, repository):
        return os.path.join(directory, repository)

    def get_repository_url(this, repository):
        return this.url_base + repository + ".git"

    def get_repository(this, directory, repository):
        return git.Repo(this.get_repository_directory(directory, repository))

    def get_all_repository_directories(this):
        return [this.get_repository_directory(this.build_directory, x) for x in this.repository_names]

    def clone_repository(this, directory, repository):
        origin = this.get_repository_url(repository)
        d = this.get_repository_directory(directory, repository)

        print(f"Origin: {origin}")
        print(f"Repo directory: {d}")
        if os.path.exists(d): raise OSError(f"Directory already exists: {d}")

        return git.Repo.clone_from(origin, d)

    def clone_repository_if_needed(this, directory, repository):
        try:
            repo = this.clone_repository(directory, repository)
        except IOError:
            repo = git.Repo(this.get_repository_directory(directory, repository))
            print(f"Repository already cloned: {repository}")

        print(f"Cloned repository {this.get_repository_url(repository)} into {directory}")
        return repo

    def clone_repositories_if_needed(this, directory):
        repositories = []
        for r in this.repository_names:
            repositories.append(this.clone_repository_if_needed(directory, r))

        return repositories

    def set_branch_and_update(this, repository, branch):
        origin = repository.remote()
        origin.fetch()

        if branch in repository.branches:
            if repository.branches[branch] is not repository.active_branch:
                print(f"Local checkout: {branch}")
                repository.branches[branch].checkout()
            else: print(f"Branch already checked out: {branch}")
        elif branch in origin.refs:
            print(f"Remote checkout: {branch}")
            repository.git.checkout('-b', branch, origin.refs[branch])
        else:
            print(f"Branch does not exist: {branch}")

        origin.pull()

    def update_all(this):
        this.create_directory_if_needed(this.build_directory)
        repos = this.clone_repositories_if_needed(this.build_directory)
        if not this.update: return

        for r in repos: this.set_branch_and_update(r, this.release)

    @staticmethod
    def from_args(args, url_base, list_of_repository_names):
            additional_names = []
            if args.additional_repositories is not None:
                additional_names = args.additional_repositories.split(",")
            if args.only is not None: repos = args.only.split(",")
            else: repos = list_of_repository_names+additional_names

            return RepositoryManager(
                    url_base,
                    args.build_directory,
                    args.release,
                    args.update,
                    repos)
