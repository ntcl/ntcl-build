! Auto-generated -- DO NOT MODIFY
program unittest
    use :: util_api, only : &
            selector, &
            string

    use :: comm_api, only : &
            mpi_initializer, &
            mpi_identity, &
            distributed_assert

${module_header}
    implicit none

    type(mpi_initializer) :: mpi
    type(mpi_identity) :: mpi_id
    type(distributed_assert) :: assertion
    type(selector) :: aselector

${run_header}
    mpi = mpi_initializer()
    mpi_id = mpi%get_mpi_identity_for_world()
    assertion = distributed_assert(mpi_id)
    aselector = selector([string("long")])

${run_statements}
    call assertion%write_summary()

    call aselector%cleanup()
    call assertion%cleanup()
    call mpi%cleanup()
end program unittest
